﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeUnits : MonoBehaviour
{
    public GameObject lifeUnit;
    public Transform character;
    public const float MAX_Y = 9f;
    public const float MIN_Y = 6f;
    public float rightX = 0;
    private void Start() {
        rightX = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x;
    }
    
    private void Update() {
        Vector3 lifeUnitOnScreenPosition = Camera.main.WorldToScreenPoint(lifeUnit.transform.position);
        if (lifeUnitOnScreenPosition.y < 0) {
            float xPosition = Random.Range(0, rightX);
            float yPosition = character.position.y + Random.Range(MIN_Y, MAX_Y);
            float zPosition = character.position.z;
            Vector3 newLifeUnitPosition = new Vector3(xPosition, yPosition, zPosition);
            lifeUnit.transform.position = newLifeUnitPosition;
        }
    }
}
