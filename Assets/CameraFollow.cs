﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public float dampTime = 0.65f;
    private Vector3 velocity = Vector3.zero;
    public Transform character;
 
    private void Start() {
        Debug.Log(Screen.height);
    }
    // Update is called once per frame
    void FixedUpdate () {
        if (character && character.position.y > transform.position.y) {
            Vector3 destination = new Vector3(transform.position.x, character.position.y, transform.position.z);
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
        }
    }
}
