﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CharacterController : MonoBehaviour
{
    const string DIRECTION_RIGHT = "right";
    const string DIRECTION_LEFT = "left";
    public string currentDirection = DIRECTION_RIGHT;
    public float velocityY = 15f;
    public float velocityX = 10f;
    public GameObject character;
    public float life = 100f;
    public GameObject healthBar;
    public float healthBarInitialScale;

    private void Start() {
        healthBarInitialScale = healthBar.transform.localScale.x;
        Debug.Log(healthBarInitialScale);
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        string colliderName = collision.collider.name;
        if (colliderName == "leftWall" || colliderName == "rightWall") {
            Debug.Log("You dead");
            Destroy(character);
        }
        if (colliderName == "floor") {
            Debug.Log("Dropped");
        }
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        if (collider.name == "cherry") {
            float newLife = life + 10;
            if (newLife > 100) {
                life = 100;
            } else {
                life = newLife;
            }
            Debug.Log("Life Supplied");
        }
    }

    private void Update() {
        if (life > 0) {
            life -= 4f * Time.deltaTime;
            healthBar.transform.localScale = new Vector3(life / 100 * healthBarInitialScale, 1, 1);
        } else {
            Destroy(character);
            Debug.Log("You dead");
        }
    }

    private void OnGUI() {
        if (Event.current.Equals(Event.KeyboardEvent("space"))) {
            Rigidbody2D rb = character.GetComponent<Rigidbody2D>();
            Vector2 newVelocity = new Vector2();
            newVelocity.y = velocityY;
            if (currentDirection == DIRECTION_RIGHT) {
                currentDirection = DIRECTION_LEFT;
                newVelocity.x = -velocityX;
            } else {
                currentDirection = DIRECTION_RIGHT;
                newVelocity.x = velocityX;
            }
            rb.velocity = newVelocity;
        }
    }
}
